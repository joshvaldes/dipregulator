# README #
[DIP regulator](https://bitbucket.org/joshvaldes/dipregulator) by [Josh Valdes](https://bitbucket.org/joshvaldes/) is licensed under a [Creative Commons Attribution-ShareAlike 4.0 International License.](http://creativecommons.org/licenses/by-sa/4.0/)

![cc.png](https://bitbucket.org/repo/4d478q/images/4103728382-cc.png)